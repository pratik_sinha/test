import json
import time
from concurrent.futures import ThreadPoolExecutor as PoolExecutor
import concurrent.futures
import requests
import threading
import get_token

thread_local = threading.local()
headers = {'Content-Type': 'application/json', 'Authorization': 'Bearer fa712a1948242a5efed6'}
proxies = {'https': 'http://clientproxy.nat.bt.com:8080'}
response = {
    "status": "Failure"
}
url = "https://ciena-rhel-alb-865032782.eu-west-2.elb.amazonaws.com:8445/engine-rest/message"


# url = "https://webhook.site/02f2a904-d189-4be5-aa72-a8c3a9673d12"


def postMessage(payload_dict):
    print(payload_dict)
    print(type(payload_dict))
    time.sleep(500)
    try:
        for key, value in payload_dict.items():

            body = {
                'messageName': 'KCI_Recieved',
                'processInstanceId': '0b4760eb-4ee4-11ec-ab83-0242ac100012',
                'processVariables': {
                    'OpenreachPayload': {
                        'type': 'json',
                        'value': json.dumps(value)
                    }
                }
            }
            result = requests.post(url=url, headers=headers, data=json.dumps(body), proxies=proxies, verify=False)
            print("result is {} for {}".format(result, key))

            if result.status_code > 300 or result.status_code < 200:
                time.sleep(3)
                result = requests.post(url=url, headers=headers, data=json.dumps(body), proxies=proxies, verify=False)
                print("attempt 2 result is {} for {}".format(result, key))
                if result.status_code > 300 or result.status_code < 200:
                    time.sleep(7)
                    result = requests.post(url=url, headers=headers, data=json.dumps(body), proxies=proxies,
                                           verify=False)
                    print("attempt 3 result is {} for {}".format(result, key))
                    if result.status_code > 300 or result.status_code < 200:
                        time.sleep(11)
                        result = requests.post(url=url, headers=headers, data=json.dumps(body), proxies=proxies,
                                               verify=False)
                        print("attempt 4 result is {} for {}".format(result, key))


    except Exception as ex:
        message = "outputs validation failed with message {}".format(ex)
        print(message)
        raise ValueError(message)


# Test_Url = "https://ciena-alb-1766728224.eu-west-2.elb.amazonaws.com:8446/engine-rest/message"

start_time = time.time()

x = 3
key_list = []
while x <= 5:
    payload_body = {
        "OrderStatusUpdate": {
            "OrderResponse": {
                "BuyerParty": {
                    "Party": {
                        "PartyIdentification": {
                            "ID": {
                                "identificationSchemeName": "DUNS",
                                "content": 522695198
                            }
                        }
                    }
                },
                "OrderReference": {
                    "SellersID": "1-168246585604",
                    "BuyersID": "CIENABP:e3c3fb2b-66f3-11eb-97da-0242ac100013"
                },
                "SellerParty": {
                    "Party": {
                        "PartyIdentification": {
                            "ID": {
                                "identificationSchemeName": "DUNS",
                                "content": 364877501
                            }
                        }
                    }
                },
                "SellersID": "1-1682465856041612448720040",
                "IssueDateTime": "2021-10-12T14:25:20.493Z",
                "RespondedOrderLine": {
                    "LineItem": {
                        "Site": {
                            "Address": {
                                "AddressReference": {
                                    "RefNum": "A90000374627"
                                }
                            },
                            "End": "B"
                        },
                        "ExternalVisit": {
                            "PreferredExternalVisitDate": "2021-02-12"
                        },
                        "Item": {
                            "SellersItemIdentification": {
                                "ID": "Generic Ethernet Access - FTTP"
                            }
                        },
                        "OrderType": "Provision",
                        "OrderSubType": "Basic Provide",
                        "Dates": {
                            "RequiredByDate": "2021-12-12T12:07:43.493Z"
                        },
                        "Features": {
                            "GEAOutput": {
                                "FTTPAvailable": "Y",
                                "SiteVisit": {
                                    "ListOfSiteVisitReason": {
                                        "SiteVisitReason": [
                                            {
                                                "Action": "Create",
                                                "Id": 106,
                                                "Code": "Install UPS",
                                                "Notes": "Please install"
                                            },
                                            {
                                                "Action": "Create",
                                                "Id": 107,
                                                "Code": "Standard",
                                                "Notes": "zzz"
                                            }
                                        ]
                                    }
                                },
                                "OHP": {
                                    "OHPExchangeCode": "GRW",
                                    "OHPExchangeName": "GREENWICH",
                                    "DistrictCode": "ST"
                                },
                                "ServiceId": "OGEA01038604"
                            },
                            "GEAInput": {
                                "ECChargeBand": 0
                            }
                        },
                        "BuyersID": 1,
                        "LineStatus": "Acknowledged"
                    },
                    "OrderLineMessageInfo": {
                        "MessageInfo": {
                            "Message": "Order Acknowledged",
                            "Severity": "Informational",
                            "Code": "3N69",
                            "CompletionCode": x
                        }
                    }
                },
                "ReceivedDateTime": "2021-10-12T14:19:09.493Z"
            }
        }
    }

    temp_dict = {"body" + str(x): payload_body}
    key_list.append(temp_dict)
    x = x + 1

# new_list = []
# for i in key_list:
#     for key, value in i.items():
#         # postMessage(payload_body=value)
#         new_list.append(value)

with PoolExecutor(max_workers=5) as executor:
    print(key_list)
    print(json.dumps(key_list))
    # time.sleep(300)
    for _ in executor.map(postMessage, key_list):
        pass

duration = time.time() - start_time
print(duration)
