import json
import requests
import time
from sendCamundaMessages import postMessage
from get_token import get_token
from concurrent.futures import ThreadPoolExecutor as PoolExecutor
import concurrent.futures
import requests
import threading


def callPostMessages(message_payload_dict):
    try:
        for key, value in message_payload_dict.get('payload').items():
            processVariables = {
                'createInventoryResponse': value
            }

            result = postMessage(message_payload_dict.get('connectDict'),
                                 message_payload_dict.get('processInstanceId'),
                                 message_payload_dict.get('messageListener'),
                                 attempts=5, **processVariables)
            print(f"for {key}: the result is {result}")

    except Exception as ex:
        message = "outputs validation failed with message {}".format(ex)
        print(message)
        raise ValueError(message)


def sendMultipleMessages(instance, process_instance_id, message_listener, start, end):
    x = start
    key_list = list()
    print(x)
    connect_dictionary = get_token(instance, )
    while x <= end:
        processVariables = {
            "serviceData": x
        }

        temp_dict = {"KCI_" + str(x): processVariables}
        temp_message_payload_dict = {
            'payload': temp_dict,
            'processInstanceId': process_instance_id,
            'messageListener': message_listener,
            'connectDict': connect_dictionary
        }
        key_list.append(temp_message_payload_dict)
        x = x + 1

    start_time = time.time()

    with PoolExecutor(max_workers=8) as executor:
        for _ in executor.map(callPostMessages, key_list):
            pass

    duration = time.time() - start_time
    print(duration)
