import json
import time
from operator import itemgetter

with open('testResults_CustomOperation.json') as f:
    data = json.load(f)
finalList_1 = list()
finalList_2 = list()
reverseFinalList = list()
custom_operation_list = data.get('items')
n = 1
for custom_operation in custom_operation_list:
    n = n + 1
    try:
        finalList_1.append(custom_operation['outputs']['MessageInfo'][0]['CompletionCode'])
        finalList_2.append(custom_operation['outputs']['MessageInfo'][0]['CompletionCode'])
    except:
        n = n + 1
        finalList_1.append(1000 + n)
finalList_1.sort()
print(finalList_1)
print(finalList_2)
print(json.dumps(finalList_1))
