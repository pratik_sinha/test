'''
This program is to fetch the oath2 tokn to log into Ciena Blue Planet
'''

import requests
import json
import os


def get_token(instance):
    dir_name = os.path.dirname(__file__)
    credentials_base_path = "{}/credentials".format(dir_name)
    credentials_full_name = os.path.join(credentials_base_path, "credentials.json")
    with open(credentials_full_name) as access_details:
        data = json.load(access_details)

    for env in data:
        if env.get('instance') == instance:
            credentials = env.get("credentials")
            base_url = env.get("base_url")

    URL_GetToken = "{}/tron/api/v1/oauth2/tokens".format(base_url)

    headers = {
        'content-type': 'application/x-www-form-urlencoded'
    }
    proxies = {'https': 'http://clientproxy.nat.bt.com:8080'}
    print("test1")
    token_data = requests.post(URL_GetToken, data=credentials, headers=headers, verify=False, proxies=proxies)

    GetTokenResponse = token_data.json()
    token = GetTokenResponse["accessToken"]
    Authorization = "Bearer {}".format(str(token))
    headers = {
        'Content-Type': 'application/json',
        'Authorization': Authorization
    }
    result = {
        'headers': headers,
        'baseUrl': base_url,
        'proxies': proxies
    }
    return result
