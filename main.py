from sendMultiThreadedMessages import sendMultipleMessages
from sendMultiThreadedMessages import callPostMessages
from get_token import get_token
import time
import os
import requests
import json


def getCamundaProcessInstanceVariables(connect_dict, process_id):
    url = "{}/engine-rest/process-instance/{}/variables?deserializeValues=false".format(connect_dict.get('baseUrl'),
                                                                                        process_id)
    result_get_pid_vars = requests.get(url=url, headers=connect_dict.get('headers'),
                                       proxies=connect_dict.get('proxies'), verify=False)
    return result_get_pid_vars.json()


instance = 'NGBB_21_02_2B'
process_instance_id = 'bae78072-6f0b-11ec-9640-0242ac100038'
message_listener = 'KciMessage'
start_range = 1
end_range = 100
connect_dictionary = get_token(instance)
sendMultipleMessages(instance, process_instance_id, message_listener, start_range, end_range)

time.sleep(120)
get_process_details = getCamundaProcessInstanceVariables(connect_dictionary, process_instance_id)

with open('data.json', 'w') as outfile:
    json.dump(get_process_details, outfile, indent=2, sort_keys=True)

list_of_strings = list()
parent_child_rel_from_list_of_json = list()
list_of_json = list()
list_of_activity_kci = list()
list_of_activity_id_data = list()
list_of_activity_kci_without_prefix = list()
for key, value in get_process_details.items():
    if value.get('type') == "String":
        list_of_strings.append({key: value.get('value')})
    if value.get('type') == "Json":
        val = json.loads(value.get('value'))
        list_of_json.append({key: val})
        if type(val) == dict:
            for k, v in val.items():
                if k == 'KCI_Payload':
                    list_of_activity_kci.append({key: v})
                    list_of_activity_kci_without_prefix.append({key.removeprefix('Activity_KCI:'): v})
                elif type(v) == dict:
                    if v.get('KCI_Payload'):
                        list_of_activity_id_data.append({key: val})
                        for ks, vs in val.items():
                            parent_child_rel_from_list_of_json.append({key: ks})

with open('list_of_strings.json', 'w') as outfile:
    json.dump(list_of_strings, outfile, indent=2, sort_keys=True)
with open('list_of_json.json', 'w') as outfile:
    json.dump(list_of_json, outfile, indent=2, sort_keys=True)
with open('list_of_activity_kci.json', 'w') as outfile:
    json.dump(list_of_activity_kci, outfile, indent=2, sort_keys=True)
with open('list_of_activity_id_data.json', 'w') as outfile:
    json.dump(list_of_activity_id_data, outfile, indent=2, sort_keys=True)
with open('list_of_activity_kci_without_prefix.json', 'w') as outfile:
    json.dump(list_of_activity_kci_without_prefix, outfile, indent=2, sort_keys=True)
with open('parent_child_rel_from_list_of_json.json', 'w') as outfile:
    json.dump(parent_child_rel_from_list_of_json, outfile, indent=2, sort_keys=True)


print(f'list_of_strings has {len(list_of_strings)} items')
print(f'list_of_json has {len(list_of_json)} items')
print(f'list_of_activity_kci has {len(list_of_activity_kci)} items')
print(f'list_of_activity_id_data has {len(list_of_activity_id_data)} items')
print(f'list_of_activity_kci_without_prefix has {len(list_of_activity_kci_without_prefix)} items')
print(f'parent_child_rel_from_list_of_json has {len(parent_child_rel_from_list_of_json)} items')
new_compare_list = list()

list_kci_code = list()
for item_loakwp in list_of_activity_kci_without_prefix:
    for k_item_loakwp, v_item_loakwp in item_loakwp.items():
        if v_item_loakwp.get('OrderStatusUpdate'):
            list_kci_code.append(v_item_loakwp['OrderStatusUpdate']['OrderResponse']['RespondedOrderLine']['OrderLineMessageInfo']['MessageInfo']['CompletionCode'])
list_kci_code.sort()
print(json.dumps(list_kci_code), '\n', len(list_kci_code))

list_kci_code_or = list()
for item_loakwp in list_of_activity_kci_without_prefix:
    for k_item_loakwp, v_item_loakwp in item_loakwp.items():
        list_kci_code_or.append(v_item_loakwp['OrderStatusUpdate']['OrderResponse']['RespondedOrderLine']['OrderLineMessageInfo']['MessageInfo']['CompletionCode'])
list_kci_code_or.sort()
print(json.dumps(list_kci_code_or), '\n', len(list_kci_code_or))


for item_loaid in list_of_activity_id_data:
    new_compare_dict = dict()
    for key_loaid, value_loaid in item_loaid.items():
        new_compare_dict['comparingActivityId_list_of_activity_id_data'] = key_loaid
        count = 0
        for item_los in list_of_strings:
            for key_los, value_los in item_los.items():
                if key_los == key_loaid:
                    new_compare_dict['parentID_list_of_strings'] = value_los
                    count = count + 1
            new_compare_dict['count_ActivityId_in_list_of_strings'] = count
        count = 0
        for item_pcrfloj in parent_child_rel_from_list_of_json:
            for key_lpcrfloj, value_pcrfloj in item_pcrfloj.items():
                if value_pcrfloj == key_loaid:
                    new_compare_dict['parent_child_rel_from_list_of_json'] = key_los
                    count = count + 1
            new_compare_dict['count_ActivityId_in_parent_child_rel'] = count
    new_compare_list.append(new_compare_dict)

print(json.dumps(new_compare_list))
