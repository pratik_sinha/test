import json
import time
from operator import itemgetter

with open('testDataDict.json') as f:
    data = json.load(f)
finalList = list()
finalDict = dict()
reverseFinalList = list()
for key, value in data.items():
    tempDict = dict()
    otherTempDict = dict()

    if value.get('KCI_Payload'):
        # data = json.loads(value.get('value'))
        data = value
        tempDict[key] = data.get('KCI_Payload').get('OrderStatusUpdate')['OrderResponse']['RespondedOrderLine'] \
            ['OrderLineMessageInfo']['MessageInfo']['CompletionCode'] if data.get('KCI_Payload') else None
        # print(tempDict)
        if data.get('KCI_Payload'):
            otherTempDict[data.get('KCI_Payload').get('OrderStatusUpdate')['OrderResponse']['RespondedOrderLine'] \
                ['OrderLineMessageInfo']['MessageInfo']['CompletionCode'] if data.get('KCI_Payload') else None] = key

        reverseFinalList.append(otherTempDict)
        finalList.append(tempDict)
        # finalDict.

print(finalList)
print(reverseFinalList)
newList = sorted(reverseFinalList, key=lambda d: list(d.keys()))
print(json.dumps(newList))
newList = sorted(finalList, key=lambda d: list(d.keys()))
print(json.dumps(finalList))
