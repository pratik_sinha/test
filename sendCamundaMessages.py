import json
import requests
import time


def postMessage(connect_dict, process_instance_id, message_listener, attempts=5, **kwargs):
    url = "{}/engine-rest/message".format(connect_dict.get('baseUrl'))
    variables = {}
    final_result = dict()
    for key, value in kwargs.items():
        if type(value) == str:
            value_type = 'String'
        elif type(value) == int:
            value_type = 'Integer'
        elif type(value) == bool:
            value_type = 'Boolean'
        elif type(value) == dict or list:
            value_type = 'Json'
            value = json.dumps(value)
        else:
            raise Exception('Unknown process variable type')

        variables[key] = {
            "type": value_type,
            "value": value,
            # "valueInfo": {
            #     "transient": True
            # }
        }
    body = {
        'messageName': message_listener,
        'processInstanceId': process_instance_id,
        'processVariables': variables
    }
    print(f'{json.dumps(body)}\n')
    for i in range(attempts):
        try:
            result_send_msg = requests.post(url=url, headers=connect_dict.get('headers'),
                                            data=json.dumps(body), proxies=connect_dict.get('proxies'),
                                            verify=False)
            final_result['attemp_{}'.format(str(i))] = result_send_msg
            if 300 > result_send_msg.status_code > 199:
                break
            else:
                time.sleep(2)

        except Exception as ex:
            message = "outputs validation failed with message {}".format(ex)
            print(message)
            raise ValueError(message)

    return final_result
